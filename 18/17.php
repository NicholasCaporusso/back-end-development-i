<?php
class Worker
{
  private $id;
  protected $name;
  protected $dept;
}
$work=new Worker(1,'James Das','Development');
$serialized=serialize($work);
echo "*Serialized object in string format:<br>".$serialized."<br><br>";
$work1=unserialize($serialized);
echo "*Object Unserialised:<br>";
$work1->display();

