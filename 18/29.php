<?php

try { 
  print "this is our try block\n";
  throw new Exception();
} catch (Exception $e) {
  print "something went wrong\n";
} finally {
  print "This part is always executed\n";
}