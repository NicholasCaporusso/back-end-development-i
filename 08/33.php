<?php
class Dog{
  public $name;
}

class Person{
	public $name;
	public $dog;
	
	public function callDog(){
		echo sprintf('%s, come here!',$this->dog->name);
	}
}

$person=new Person;
$person->dog=$dog=new Dog;
$person->dog->name='Lassie';

$person->callDog();