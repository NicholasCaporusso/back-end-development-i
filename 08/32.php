<?php

class Dog {
  public $name;
  private $age;
  public $breed;
  private $owner;
  
  public function __construct($name){
     $this->name=$name;
  }
  public function __destruct(){
     echo 'bye';
  }

  public function eat(){
	$this->breed='Labrador';
    $this->bark();
    echo 'yum';
  }
  private function bark(){
    echo 'woff';
  }
  function play(){}
  function catch_ball(){}
}

$dog=new Dog('Lassie');
$dog=null;