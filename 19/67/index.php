<pre><?php
$base='API';
$URI=preg_replace(array(sprintf('/^\/?%s\//i',$base),'/\/?$/i'),'',$_SERVER['REQUEST_URI']);

$rules=array(
	'vehicles(\/(.+))*'=>'vehicles/index',
	'houses(\/(.+))*'=>'houses/index'
);

route($URI,$rules);

function route($URI,$rules){
	foreach($rules as $p=>$m) if(preg_match('/'.$p.'/',$URI)){
		require_once($m.'.php');
		break;
	}
}