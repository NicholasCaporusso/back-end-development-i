<pre><?php

if(!isset($_REQUEST['user_token']{0})){
	if(!isset($_REQUEST['username']{0}) || !isset($_REQUEST['password']{0})) die('please enter username and password');
	else{
		if($_REQUEST['username']=='user' && $_REQUEST['password']!='pass'){
			die(json_encode(['token'=>'randomtoken'])); // replace with randomly generated token which will be stored in the database
		}else die('wrong username/password');
		
	}
}else if($_REQUEST['user_token']!='randomtoken') die('authentication error');


$base='API';
$URI=preg_replace(array(sprintf('/^\/?%s\//i',$base),'/\/?$/i'),'',$_SERVER['REQUEST_URI']);

$rules=array(
	'vehicles(\/(.+))*'=>'vehicles/index',
	'houses(\/(.+))*'=>'houses/index'
);

route($URI,$rules);

function route($URI,$rules){
	foreach($rules as $p=>$m) if(preg_match('/'.$p.'/',$URI)){
		require_once($m.'.php');
		break;
	}
}