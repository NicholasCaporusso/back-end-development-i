<?php
$data=array(
	array('username'=>'Zebra11','name'=>'Waylon Dalton','age'=>30),
	array('username'=>'Snake19','name'=>'Justine Henderson','age'=>28),
	array('username'=>'Horse84','name'=>'Abdullah Lang','age'=>32),
	array('username'=>'Cow10','name'=>'Marcus Cruz','age'=>29),
	array('username'=>'Snake56','name'=>'Thalia Cobb','age'=>24),
	array('username'=>'Zebra83','name'=>'Mathias Little','age'=>22),
	array('username'=>'Fox99','name'=>'Eddie Randolph','age'=>26),
	array('username'=>'Elephant45','name'=>'Lia Shelton','age'=>28),
	array('username'=>'Tiger21','name'=>'Hadassah Hartman','age'=>35),
	array('username'=>'Koala41','name'=>'Joanna Shaffer','age'=>21),
	array('username'=>'Cat18','name'=>'Jonathon Sheppard','age'=>19),
);
header('Content-Type: application/json');
echo json_encode($data);
die();