<?php
   $file_write = "write_mode.txt";
   $file_append = "append_mode.txt";
   $file = fopen( $file_write, "w+" );
   fwrite( $file, "This is a simple test\n" );
   fclose( $file );
   
   $file = fopen( $file_append, "a+" );
   fwrite( $file, "This is append mode\n" );
   fclose( $file );
?>