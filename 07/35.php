<?php
if(isset($_POST['submit'])){
	$error=false;
	$target_dir = "";
	$target_file = $target_dir . basename($_FILES["one_file"]["name"]);
	if (file_exists($target_file)) {
		echo "Sorry, file already exists.";
		$error=true;
	}
	if ($_FILES["one_file"]["size"] > 500000) {
		echo "Sorry, your file is too large.";
		$error=true;
	}
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
		echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		$error=true;
	}
    if (!$error && move_uploaded_file($_FILES["one_file"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["one_file"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
?>
<html>
   <body>
      
      <form action="35.php" method="POST" enctype="multipart/form-data">
         <input type="file" name="one_file" />
         <input type="submit" name="submit" />
      </form>
      
   </body>
</html>