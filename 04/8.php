<?php
   function birthday()
   {
      // initialize once
      static $age = 0;
      return( ++$age );

      // would this result in the same value?
      // return( $age++ );
   }

   for ( $i = 0 ; $i < 5 ; $i++ )
      printf( "Age: %d<br />", birthday() );
?>
