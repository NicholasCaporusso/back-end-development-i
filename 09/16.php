<?php
class Father {
  // The parent’s class code
}
 
class Child extends Father {
  // The  child can use the parent's class code
}
