<?php
interface interfaceName { 
  // abstract methods
}
 
class Child implements interfaceName {
  // defines the interface methods and may have its own code
}